﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Polly.Retry;
using System.Net.Http;
using System.Threading.Tasks;
using Polly;
using Polly.Retry;
using Polly.CircuitBreaker;
using System;

namespace BlogServicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogGatewayController : ControllerBase
    {
        private readonly HttpClient _httpClient;
        private readonly AsyncRetryPolicy _retryPolicy;
        private readonly AsyncCircuitBreakerPolicy _circuitBreakerPolicy;

        public BlogGatewayController(HttpClient httpClient)
        {
            _httpClient = httpClient;

            // Retry policy that will retry the request 3 times if it fails
            _retryPolicy = Policy
                .Handle<HttpRequestException>()
                .RetryAsync(3);

            // Circuit breaker policy that will stop trying the request for 1 minute after 3 failures
            _circuitBreakerPolicy = Policy
                .Handle<HttpRequestException>()
                .CircuitBreakerAsync(3, TimeSpan.FromMinutes(1));
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerListaPost()
        {
            HttpResponseMessage response = null;
            await _retryPolicy.ExecuteAsync(async () =>
            {
                response = await _httpClient.GetAsync("http://localhost:5001/api/Blog");
                response.EnsureSuccessStatusCode();
            });

            var content = await response.Content.ReadAsStringAsync();
            return new OkObjectResult(content);
        }
    }
}
