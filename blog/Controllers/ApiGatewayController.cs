﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlogServicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiGatewayController : ControllerBase
    {
        private readonly HttpClient _httpClient;

        public ApiGatewayController(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        [HttpGet("blog")]
        public async Task<IActionResult> BlogGateway()
        {
            // Obtener respuesta del microservicio correspondiente
            var response = await _httpClient.GetAsync("http://localhost:5001/api/Blog");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();


            return new OkObjectResult(content);
        }


    }
}
