﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlogServicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogBFFController : ControllerBase
    {
        private readonly HttpClient _httpClient;

        public BlogBFFController(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        [HttpGet]
        public async Task<IActionResult> ObtenerListaPost()
        {
            var response = await _httpClient.GetAsync("http://localhost:5002/api/Blog");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return new OkObjectResult(content);
        }
    }
}
