﻿using BlogDominio.Entidades;
using BlogRepositorio.Repositorio;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogServicio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IRepositorioPost _repositorioPost;
        private readonly IEventBus _eventBus; // Event Bus para publicar eventos

        public BlogController(IRepositorioPost repositorioPost, IEventBus eventBus)
        {
            _repositorioPost = repositorioPost;
            _eventBus = eventBus;
        }

        [HttpGet]
        public IActionResult ObtenerListaPost()
        {
            var posts = _repositorioPost.ObtenerListaPost();
            return new OkObjectResult(posts);
        }

        [HttpGet("{id}", Name = "ObtenerListaPost")]
        public IActionResult ObtenerListaPostByID(int id)
        {
            var post = _repositorioPost.ObtenerListaPostByID(id);
            return new OkObjectResult(post);
        }

        [HttpPost]
        public IActionResult RegistrarPost([FromBody] Post post)
        {
            using (var scope = new TransactionScope())
            {
                _repositorioPost.RegistrarPost(post);

                // Publicar evento de creación de post
                var eventoCreacionPost = new EventoCreacionPost(post.Id, post.Titulo, post.Descripcion);
                _eventBus.PublicarEvento(eventoCreacionPost);

                scope.Complete();
                return CreatedAtAction(nameof(ObtenerListaPost), new { id = post.Id }, post);
            }
        }

        [HttpPut]
        public IActionResult ActualizarPost([FromBody] Post post)
        {
            if (post != null)
            {
                using (var scope = new TransactionScope())
                {
                    _repositorioPost.ActualizarPost(post);

                    // Publicar evento de actualización de post
                    var eventoActualizacionPost = new EventoActualizacionPost(post.Id, post.Titulo, post.Descripcion);
                    _eventBus.PublicarEvento(eventoActualizacionPost);

                    scope.Complete();
                    return new OkResult();
                }
            }
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult EliminarPost(int id)
        {
            _repositorioPost.EliminarPost(id);

            // Publicar evento de eliminación de post
            var eventoEliminacionPost = new EventoEliminacionPost(id);
            _eventBus.PublicarEvento(eventoEliminacionPost);

            return new OkResult();
        }
    }
}
