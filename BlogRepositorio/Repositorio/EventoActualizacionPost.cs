﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogRepositorio.Repositorio
{
    public class EventoActualizacionPost : IEvento
    {
        public Guid Id { get; }
        public DateTime Fecha { get; }
        public long PostId { get; }
        public string NuevoTitulo { get; }
        public string NuevoContenido { get; }

        public EventoActualizacionPost(long postId, string nuevoTitulo, string nuevoContenido)
        {
            Id = Guid.NewGuid();
            Fecha = DateTime.UtcNow;
            PostId = postId;
            NuevoTitulo = nuevoTitulo;
            NuevoContenido = nuevoContenido;
        }
    }
}
