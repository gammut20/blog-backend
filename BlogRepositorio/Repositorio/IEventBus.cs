﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogRepositorio.Repositorio
{
    public interface IEventBus
    {
        void PublicarEvento<TEvento>(TEvento evento) where TEvento : IEvento;
        void SuscribirseEvento<TEvento, TManejadorEvento>()
            where TEvento : IEvento
            where TManejadorEvento : IManejadorEvento<TEvento>;
    }

    public interface IEvento
    {
        Guid Id { get; }
        DateTime Fecha { get; }
    }

    public interface IManejadorEvento<TEvento> where TEvento : IEvento
    {
        void ManejarEvento(TEvento evento);
    }
}
