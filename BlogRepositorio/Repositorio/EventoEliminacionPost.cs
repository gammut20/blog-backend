﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogRepositorio.Repositorio
{
    public class EventoEliminacionPost : IEvento
    {
        public Guid Id { get; }
        public DateTime Fecha { get; }
        public long PostId { get; }

        public EventoEliminacionPost(long postId)
        {
            Id = Guid.NewGuid();
            Fecha = DateTime.UtcNow;
            PostId = postId;
        }
    }
}
