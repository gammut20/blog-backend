﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogRepositorio.Repositorio
{
    public class EventoCreacionPost : IEvento
    {
        public Guid Id { get; }
        public DateTime Fecha { get; }
        public long PostId { get; }
        public string Titulo { get; }
        public string Contenido { get; }

        public EventoCreacionPost(long postId, string titulo, string contenido)
        {
            Id = Guid.NewGuid();
            Fecha = DateTime.UtcNow;
            PostId = postId;
            Titulo = titulo;
            Contenido = contenido;
        }
    }
}
